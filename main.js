import { AUTO, Game, Geom } from 'phaser';
import GameOver from './src/scenes/GameOver';
import Level from './src/scenes/Level';
import Menu from './src/scenes/Menu'


const config = {
	width: 512,		// largura
	height: 750,	// altura
	type: AUTO,
	scene: [Menu, Level, GameOver],
	physics: {
		default: 'arcade',
		arcade: {
			gravity: {
				y: 100
			},
			debug: true
		}
	}
}

new Game(config);