import { Scene } from "phaser";

export default class Menu extends Scene 
{

    constructor() 
    {
        super('menu');
    }

    create() 
    {
        let width = this.scale.width

        this.add.text(width/2, 50, 'Flappy Bird',{
            fontSize:25
        }).setOrigin(0.5);

        this.add.text(width/2, 250, 'Press L to Light theme',{
            fontSize:25
        }).setOrigin(0.5);
        this.add.text(width/2, 450, 'Pressione D to Dark theme',{
            fontSize:25
        }).setOrigin(0.5);

        this.add.text(width/2, 650, 'Press SPACE to control the bird',{
            fontSize:20
        }).setOrigin(0.5);


        this.input.keyboard.on('keydown-L', () => {
            this.scene.start('level', {theme: 'light'});
        });

        this.input.keyboard.on('keydown-D', () => {
            this.scene.start('level', {theme: 'dark'});
        });
    }
}