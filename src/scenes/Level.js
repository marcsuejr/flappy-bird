// Level.js
import { Math, Scene, Geom } from "phaser";

export default class Level extends Scene 
{
 
     /** @type {Phaser.Physics.Arcade.Sprite} */
     bird;

     /** @type {Phaser.Physics.Arcade.StaticGroup} */
     pipesUp;

     /** @type {Phaser.Physics.Arcade.StaticGroup} */
     pipesDown;

     points;

     /**@type {Phaser.GameObjects.Text} */
     pointsText;
     
     constructor() 
     {
         super('level');
     }
    
     init(data) 
     {
        this.theme = data.theme;
     }
 
     preload()
     {
         this.load.image('background-day', 'assets/background-day2.png')
         this.load.image('background-nigth', 'assets/background-night2.png')
         this.load.image('blue-bird-up', 'assets/bluebird-upflap2.png')
         this.load.image('blue-bird-down', 'assets/bluebird-downflap2.png')
         this.load.image('pipe-up', 'assets/pipeUp2.png')
         this.load.image('pipe-down', 'assets/pipeDown2.png')
          
     }
 
     create()
     { 
        this.points = 0 

        if (this.theme === 'dark') 
            this.add.image(144,256, 'background-nigth').setScrollFactor(0, 0);
        else
            this.add.image(144,256, 'background-day').setScrollFactor(0, 0);
            
        
        this.bird = this.physics.add.image(-150, 200, 'blue-bird-up')
        this.bird.setVelocityX(50);
        this.bird.setScale(1);

        this.pipesUp = this.physics.add.staticGroup();
        this.pipesDown = this.physics.add.staticGroup();

        for (let i=0; i <5 ;i++)
        {
            const pipeUpX = 150*i
            const pipeUpY = Math.Between(-50,50);
            const pipeUp = this.pipesUp.create(pipeUpX, pipeUpY, 'pipe-up')
            pipeUp.body.updateFromGameObject();


            const pipeDownX = pipeUpX
            const pipeDownY = pipeUpY+550;
            const pipeDown = this.pipesDown.create(pipeDownX, pipeDownY, 'pipe-down')
            pipeDown.body.updateFromGameObject();

        }

        this.cameras.main.startFollow(this.bird);

        this.physics.add.overlap(this.bird, this.pipesUp, this.collided, undefined, this);
        this.physics.add.overlap(this.bird, this.pipesDown, this.collided, undefined, this);

        const style = {color: '8000', fontSize: 24};
        this.pointsText= this.add.text(240,10, 'Score: '+this.points,style);
        this.pointsText.setScrollFactor(0) 
        this.pointsText.setOrigin(0.5,0) 

     }

     update()
     {
        this.input.keyboard.once('keydown-SPACE', () => {
            this.bird.setVelocityY(-100);
            
        });

        this.pipesUp.getChildren().forEach((pipeUp) => {
            if (pipeUp.getBounds().right < this.bird.getBounds().left && !pipeUp.scored) 
            {
                pipeUp.scored = true;
                this.points++;
                this.pointsText.text = 'Score: '+this.points
            }
        });

        this.pipesUp.getChildren().forEach((pipeUp, index) => {
            if (pipeUp.getBounds().right < this.cameras.main.worldView.left) 
            {
                pipeUp.x += 150 * 5;
                pipeUp.y = Math.Between(-50, 50);
                pipeUp.scored = false;
                pipeUp.body.updateFromGameObject();

                const pipeDown = this.pipesDown.getChildren()[index];
                pipeDown.x = pipeUp.x;
                pipeDown.y = pipeUp.y + 550;
                pipeDown.body.updateFromGameObject();
            }
        });


        if (this.bird.body.velocity.y > 0) 
        {
            this.bird.setTexture('blue-bird-down');
        } 
        
        else 
        {
            this.bird.setTexture('blue-bird-up');
        }

        if (this.bird.y <= 0 || this.bird.y>600)
        {
            this.collided()
        } 
     }

     collided(){  
        this.scene.start('game-over',{points: this.points});        
    }
}


