//GameOver.js
import { Scene } from "phaser";

export default class GameOver extends Scene
{
    constructor()
    {
        super('game-over');
    }

    init(data) 
    {
        this.points = data.points;
    }

    create()
    {
        let width = this.scale.width
        let height = this.scale.height

        this.add.text(width/2, 300, 'GAME OVER',{
            fontSize:40
        }).setOrigin(0.5);

        this.add.text(width/2, height/2,'Score: '+this.points,{
            fontSize:30
        }).setOrigin(0.5);
      

        this.add.text(width/2, 600, 'PRESS SPACE TO TRY AGAIN',{
            fontSize:30
        }).setOrigin(0.5);

        this.input.keyboard.once('keydown-SPACE',()=>{
            this.scene.start('menu')
        });
    }

    

}